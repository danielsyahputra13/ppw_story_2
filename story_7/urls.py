from django.urls import path
from . import views

app_name= 'story_7'

urlpatterns = [
    path('', views.index, name='story_7'),
]