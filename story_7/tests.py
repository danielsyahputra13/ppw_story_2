from django.test import TestCase, Client
from django.test.testcases import LiveServerTestCase
from django.urls import reverse

import time
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

# Create your tests here.


class UrlsPathTest(TestCase):
    def setUp(self):
        self.story7_page = reverse('story_7:story_7')

    def test_story7_page_matching_with_the_right_path(self):
        self.assertEqual(self.story7_page, '/story_7/')


class Story7PageTest(TestCase):
    def setUp(self):
        self.story7_page = reverse('story_7:story_7')

    def test_story7_page_exist(self):
        response = Client().get(self.story7_page)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'FAQ')
        self.assertTemplateUsed(response, 'story_7/index.html')

class Story7FunctionalTest(LiveServerTestCase):
    def setUp(self) -> None:
        super().setUp()
        chrome_options= webdriver.ChromeOptions()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        # self.selenium = webdriver.Chrome()

    
    def tearDown(self) -> None:
        self.selenium.quit()
        super().tearDown()

    def test_story7_page_title(self):
        self.selenium.get(self.live_server_url + '/story_7/')
        time.sleep(3)
        self.assertEqual(self.selenium.title, "Story 7 Daniel")

    def test_story7_accordion_element(self):
        """
        Test untuk mengetest display dari content accordionnya.
        Default display = none
        Setelah di klik seharusnya jadi 'block'
        """
        self.selenium.get(self.live_server_url + '/story_7/')

        accordion_items_1 = self.selenium.find_element_by_css_selector('.accordion-1 > .accordion-header > .accordion-title')
        accordion_items_2 = self.selenium.find_element_by_css_selector('.accordion-2 > .accordion-header > .accordion-title')
        accordion_items_3 = self.selenium.find_element_by_css_selector('.accordion-3 > .accordion-header > .accordion-title')
        accordion_items_4 = self.selenium.find_element_by_css_selector('.accordion-4 > .accordion-header > .accordion-title')

        # Tes apakah default dari content accordion adalah 'none'
        accordion_content_1 = self.selenium.find_element_by_css_selector('.accordion-1 > .accordion-content')
        accordion_content_2 = self.selenium.find_element_by_css_selector('.accordion-2 > .accordion-content')
        accordion_content_3 = self.selenium.find_element_by_css_selector('.accordion-3 > .accordion-content')
        accordion_content_4 = self.selenium.find_element_by_css_selector('.accordion-4 > .accordion-content')
        self.assertEqual('none', accordion_content_1.value_of_css_property('display'));
        self.assertEqual('none', accordion_content_2.value_of_css_property('display'));
        self.assertEqual('none', accordion_content_3.value_of_css_property('display'));
        self.assertEqual('none', accordion_content_4.value_of_css_property('display'));


        # Kita klik setiap header setiap accordion untuk ngetest apakah display content nya jadi 'block'
        accordion_items_1.click()
        time.sleep(2)
        accordion_items_2.click()
        time.sleep(2)
        accordion_items_3.click()
        time.sleep(2)
        accordion_items_4.click()
        time.sleep(2)

        self.assertEqual('block', accordion_content_1.value_of_css_property('display'));
        self.assertEqual('block', accordion_content_2.value_of_css_property('display'));
        self.assertEqual('block', accordion_content_3.value_of_css_property('display'));
        self.assertEqual('block', accordion_content_4.value_of_css_property('display'));

        # Klik lagi seharusnya display nya jad 'none'
        accordion_items_1.click()
        time.sleep(2)
        accordion_items_2.click()
        time.sleep(2)
        accordion_items_3.click()
        time.sleep(2)
        accordion_items_4.click()
        time.sleep(2)

        self.assertEqual('none', accordion_content_1.value_of_css_property('display'));
        self.assertEqual('none', accordion_content_2.value_of_css_property('display'));
        self.assertEqual('none', accordion_content_3.value_of_css_property('display'));
        self.assertEqual('none', accordion_content_4.value_of_css_property('display'));

    def test_story7_up_button(self):
        """
        Tes apakah tombol up nya berfungsi.
        Kalau kita up accordion-2 (order = 2) maka accordion yang di up order nya jadi 1.
        """
        self.selenium.get(self.live_server_url + "/story_7/")
        accordion_items_3 = self.selenium.find_element_by_class_name('accordion-3')
        tombol_up_accordion_3 = self.selenium.find_element_by_css_selector('.accordion-3 > .accordion-header > .button-group > .up')
        self.assertEqual('3', accordion_items_3.value_of_css_property('order'))

        # Klik tombol up
        tombol_up_accordion_3.click()
        time.sleep(2)
        self.assertEqual("2", accordion_items_3.value_of_css_property('order'))

    def test_story7_down_button(self):
        """
        Tes apakah tombol down nya berfungsi.
        Kalau kita down accordion-1 (order = 1) maka accordion yang di down ini ordernya jadi 2
        """
        self.selenium.get(self.live_server_url + '/story_7/')
        accordion_items_1 = self.selenium.find_element_by_class_name('accordion-1')
        tombol_down_accordion_1 = self.selenium.find_element_by_css_selector('.accordion-1 > .accordion-header > .button-group > .down')
        self.assertEqual('1', accordion_items_1.value_of_css_property('order'))

        # Klik tombol down nyaa
        tombol_down_accordion_1.click()
        time.sleep(2)
        self.assertEqual('2', accordion_items_1.value_of_css_property('order'))