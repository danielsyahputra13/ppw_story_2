from django.db import models

# Create your models here.
class Book(models.Model):
    book_id= models.CharField(max_length=200)
    book_title= models.CharField(max_length=200)
    book_author= models.CharField(max_length=200)
    book_publisher= models.CharField(max_length=200)
    book_publishedDate= models.CharField(max_length=200)
    book_rating= models.CharField(max_length=200)
    book_image= models.CharField(max_length=200)
    book_description= models.CharField(max_length=200)
    book_link= models.CharField(max_length=200)
    number_of_likes = models.IntegerField()
    def __str__(self) -> str:
        return f"{self.book_title}"