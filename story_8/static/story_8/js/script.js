$(document).ready(() => {
    const postData = (dataOfBook) => {
        const UrlLink = '/story_8/post/';
        $.post(UrlLink, {
            csrfmiddlewaretoken: $("meta[name='csrf']").attr("content"),
            id: dataOfBook.id,
            title: dataOfBook.title,
            author: dataOfBook.author,
            publisher: dataOfBook.publisher,
            publishedDate: dataOfBook.publishedDate,
            rating: dataOfBook.rating,
            image: dataOfBook.image,
            description: dataOfBook.description,
            link: dataOfBook.link,
        });
    }

    // Fungsi untuk meng-generate setiap buku nya.
    const createRow = (dataOfBook) => {
        const HTMLText = 
        `
        <div class="d-flex align-items-start p-2 border-top border-bottom">
            <div class="d-flex justify-content-center">
                <img class="rounded" style="width:200px;" src="${dataOfBook.image}" alt="">
            </div>
            <div class="d-flex flex-column ml-1 ml-md-3 col-md-12 col-10">
                <a href="${dataOfBook.link}" class="text-dark" target="_blank">
                    <h3 class="text-truncate col-9 p-0">${dataOfBook.title}</h3>
                </a>
                <p class="mb-1 col-10 p-0">Author: ${dataOfBook.author}</p>
                <p class="mb-1 col-10 p-0">Publisher: ${dataOfBook.publisher}</p>
                <p class="mb-1 col-10 p-0">Publisher: ${dataOfBook.publishedDate}</p>
                <p class="mb-1 col-10 p-0">Rating: ${dataOfBook.rating}</p>
                <p><button type="button" class="btn btn-outline-danger btn-likes">&hearts;</button></p>
            </div>
        </div>`
        return HTMLText;
    }

    const search_book = () => {
        let bookName = $('#search-book').val();
        if (bookName.length > 0) {
            $('#button-search').attr("disabled", false);
        } else {
            $('#button-search').attr("disabled", true);
        }
        if (bookName === "") bookName = 'web programming';
        const UrlLink = '/story_8/search/' + bookName;
        $.ajax({
            url: UrlLink,
            success: function (data) {
                let books = data.items;
                $('#display-book').empty()
                books.forEach((book) => {
                    const dataOfBook = {
                        id: book.id,
                        title: book.volumeInfo.title,
                        author: !book.volumeInfo.authors ? "Unknown": book.volumeInfo.authors.join(", "),
                        publisher: !book.volumeInfo.publisher ? "Unknown" : book.volumeInfo.publisher,
                        publishedDate: !book.volumeInfo.publishedDate ? "Unknown" : book.volumeInfo.publishedDate,
                        rating: book.volumeInfo.averageRating != undefined ? book.volumeInfo.averageRating : "-",
                        image: book.volumeInfo.imageLinks,
                        description: book.volumeInfo.description,
                        link: book.volumeInfo.infoLink
                    }
                    if (dataOfBook.image) dataOfBook.image = dataOfBook.image.thumbnail;
                    else dataOfBook.image = $("meta[name='images']").attr("content") + "3024051.jpg";
                    const HTMLText = createRow(dataOfBook);
                    $('#display-book').append(HTMLText);
                })
            }
        })
    }
    search_book();
    $('#search-book').keyup(() => {
        search_book();
    }) 
});