# Generated by Django 3.1.2 on 2020-12-05 00:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('story_8', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='book',
            name='number_of_likes',
            field=models.IntegerField(default=0),
            preserve_default=False,
        ),
    ]
