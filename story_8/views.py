from story_8.models import Book
from django.http.response import JsonResponse
from django.shortcuts import redirect, render
import requests

# Create your views here.
def index(request):
    return render(request, 'story_8/index.html')

def search_book(request, query):
    API_LINK = "https://www.googleapis.com/books/v1/volumes?q=" + query
    data = requests.get(API_LINK).json()
    return JsonResponse(data)

def post_like_data(request):
    if request.method == "POST":
        book_id = request.POST['id']
        try:
            book = Book.objects.get(book_id = book_id)
            num_of_likes = book.number_of_likes
            num_of_likes += 1
            book.number_of_likes = num_of_likes
            book.save()
        except Exception as ex:
            Book.objects.create(
                book_id = request.POST['id'],
                book_title = request.POST['title'],
                book_author = request.POST['author'],
                book_publisher = request.POST['publisher'],
                book_publishedDate = request.POST['publishedDate'],
                book_rating = request.POST['rating'],
                book_image = request.POST['image'],
                book_description = request.POST['description'],
                book_link = request.POST['link'],
                number_of_likes = 1,
            )
    return redirect("story_8:story_8")