from story_8.models import Book
from django.test import TestCase, Client
from django.test.testcases import LiveServerTestCase
from django.urls import reverse

import time
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

# Create your tests here.

class UrlsPathStory8Test(TestCase):
    def setUp(self) -> None:
        self.story8_page = reverse('story_8:story_8')
    
    def test_story7_page_matching_with_the_right_path(self):
        self.assertEqual(self.story8_page, '/story_8/')

class Story8PageTest(TestCase):
    def setUp(self) -> None:
        self.story8_page = reverse('story_8:story_8')

    def test_story8_page_exist(self):
        response = Client().get(self.story8_page)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'story_8/index.html')
        self.assertContains(response, "Book Catalog")

class PostViewsTest(TestCase):
    def setUp(self):
        self.post_views = reverse('story_8:post')
        self.book = Book.objects.create(
            book_id = "Book1",
            book_title = "Title1",
            book_author = "Author1",
            book_publisher = "Publisher1",
            book_publishedDate = "PublishedData1",
            book_rating = "Rating1",
            book_image = "Image1",
            book_description = "Desc1",
            book_link = "Link1",
            number_of_likes = 1,
        )
    
    def test_POST_post_views(self):
        Client().post(self.post_views, {
            "id": "Book1",
            "title": "Title1",
        })
        self.assertEqual(Book.objects.count(), 1)
        book1 = Book.objects.get(book_id = "Book1")
        self.assertEqual(book1.number_of_likes, 2)
        Client().post(self.post_views, {
            "id": "Book2",
            "title": "Title2",
            "author": "Author1",
            "publisher" : "Publisher1",
            "publishedDate" : "PublishedData1",
            "rating" : "Rating1",
            "image" : "Image1",
            "description" : "Desc1",
            "link" : "Link1",
        })
        self.assertEqual(Book.objects.count(), 2)
        book_2 = Book.objects.get(book_id = "Book2")
        self.assertEqual(book_2.number_of_likes, 1 )

class BookModelsTest(TestCase):
    def setUp(self):
        self.book = Book.objects.create(
            book_id = "Book1",
            book_title = "Title1",
            book_author = "Author1",
            book_publisher = "Publisher1",
            book_publishedDate = "PublishedData1",
            book_rating = "Rating1",
            book_image = "Image1",
            book_description = "Desc1",
            book_link = "Link1",
            number_of_likes = 1,
        )
    
    def test_models_true(self):
        self.assertEqual(Book.objects.count(), 1)
        self.assertEqual(str(self.book), "Title1")

class APICallTestCase(TestCase):
    def setUp(self) -> None:
        self.search_page = '/story_8/search/frozen'
    
    def test_search_call(self):
        response = self.client.get('/story_8/search/' + 'abcd/') 
        content = response.content.decode("utf-8")
        self.assertEqual(response.status_code, 200)
        self.assertIn("items", content)
        self.assertEqual(response['content-type'], 'application/json')


class Story8FunctionalTest(LiveServerTestCase):
    def setUp(self) -> None:
        super().setUp()
        chrome_options= webdriver.ChromeOptions()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        # self.selenium = webdriver.Chrome()

    def tearDown(self) -> None:
        self.selenium.quit()
        super().tearDown()

    def test_story8_page_title(self):
        self.selenium.get(self.live_server_url + '/story_8/')
        time.sleep(3)
        self.assertEqual(self.selenium.title, "Story 8 Daniel")

    def test_story8_search_box_element_works(self):
        self.selenium.get(self.live_server_url + '/story_8/')
        search_box = self.selenium.find_element_by_id('search-book')
        search_box.send_keys("frozen")
        time.sleep(3)
        self.assertIn( "Book Catalog", self.selenium.page_source)