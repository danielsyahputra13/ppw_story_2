from django.urls import path
from . import views

app_name= 'story_9'

urlpatterns = [
    path('signup/', views.sign_up_page, name='signup'),
]