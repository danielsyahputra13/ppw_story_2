from django.test import TestCase
from django.contrib.auth.models import User
from django.test.testcases import LiveServerTestCase
from django.urls import reverse

import time
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

# Create your tests here.
class UrlsPathTest(TestCase):
    def setUp(self) -> None:
        self.login_page = reverse('login')

    def test_login_page(self):
        response = self.client.get(self.login_page)
        self.assertEqual(self.login_page, '/accounts/login/')
        self.assertTemplateUsed(response, 'registration/login.html')
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Login')

class UserModelTest(TestCase):
    def setUp(self) -> None:
        self.user = User.objects.create_user(
            username="danielsyahputra",
            email="daniel@email.com",
            password='12345678'
        )
    
    def test_user_count(self):
        self.assertEqual(User.objects.count(), 1)
        User.objects.create(
            username='test',
            email='test@email.com',
            password='111111'
        )
        self.assertEqual(User.objects.count(), 2)
    
    def test_user_field(self):
        self.assertEqual(self.user.username, 'danielsyahputra')
        self.assertEqual(self.user.email, 'daniel@email.com')

class SignUpPageTest(TestCase):
    def setUp(self) -> None:
        self.signup_page = reverse('story_9:signup')
        self.user = User.objects.create_user(
            username='testuser',
            email= 'test@email.com',
            password='12345678'
        )
    
    def test_GET_signup_page(self):
        response = self.client.get(self.signup_page)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'story_9/signup.html')
        self.assertContains(response, 'Create a new account')

    def test_POST_signup_page_valid(self):
        response = self.client.post(
            self.signup_page, {
            'username': 'daniel123',
            'password1': 'awertyuh67',
            'password2': 'awertyuh67',
            }, follow=True
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(User.objects.count(), 2)
        self.assertContains(response, 'Login')

class Story8FunctionalTest(LiveServerTestCase):
    def setUp(self) -> None:
        super().setUp()
        chrome_options= webdriver.ChromeOptions()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        # self.selenium = webdriver.Chrome()
        self.user = User.objects.create_user(
            username='testuser',
            email= 'test@email.com',
            password='12345678'
        )

    def tearDown(self) -> None:
        self.selenium.quit()
        super().tearDown()

    def test_story9_page_title(self):
        self.selenium.get(self.live_server_url + '/accounts/login/')
        time.sleep(2)
        self.assertEqual(self.selenium.title, "Story 9 Daniel - Login")
        time.sleep(2)
        self.selenium.get(self.live_server_url + '/accounts/signup/')
        self.assertEqual(self.selenium.title, "Story 9 Daniel - Sign Up")

    def test_login_button(self):
        self.selenium.get(self.live_server_url + '/accounts/login/')
        username_box = self.selenium.find_element_by_name('username')
        password_box = self.selenium.find_element_by_name('password')
        submit_box = self.selenium.find_element_by_class_name('btn-class')
        username_box.send_keys('testuser')
        password_box.send_keys('12345678')
        submit_box.submit()
        time.sleep(4)
        self.assertEqual(self.selenium.title, "Homepage")
