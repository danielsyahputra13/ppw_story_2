from story_9.forms import CreateUserForm
from django.shortcuts import redirect, render

# Create your views here.
def sign_up_page(request):
    form = CreateUserForm()
    if request.method == "POST":
        form = CreateUserForm(request.POST)
        if form.is_valid():
            form.save()
            user = form.cleaned_data.get("username")
            return redirect('login')
    context = {'form':form}
    return render(request, 'story_9/signup.html', context)