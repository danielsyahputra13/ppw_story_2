from django.db.models.aggregates import Max
from django.test import TestCase, Client
from .models import Aktifitas, Member
# Create your tests here.

class Story6TestCase(TestCase):

    def setUp(self):

        # Di setUp ini kita ngebuat sample data ke database

        # Create activity
        aktivitas1 = Aktifitas.objects.create(deskripsi="deskripsi1", nama_aktivitas="aktivitas1")
        aktivitas2 = Aktifitas.objects.create(deskripsi="deskripsi2", nama_aktivitas="aktivitas2")

        member1 = Member.objects.create(
            nama_depan_member = 'nama1',
            nama_belakang_member= 'nama1a'
        )

        member2 = Member.objects.create(
            nama_depan_member = 'nama2',
            nama_belakang_member= 'nama2a'
        )
        member3 = Member.objects.create(
            nama_depan_member = 'nama3',
            nama_belakang_member= 'nama3a'
        )

        aktivitas1.member_aktivitas.add(member1)
        aktivitas1.member_aktivitas.add(member2)
        aktivitas2.member_aktivitas.add(member3)

    def test_models_member(self):
        member = Member.objects.get(nama_depan_member = 'nama1')
        self.assertEquals(str(member), 'nama1 nama1a')

        
    def test_index(self):
        """Cek apakah halaman awal nya sesuai"""
        c = Client()
        response = c.get("/story_6/")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['activities'].count(), 2)

    def test_aktivitas_count(self):
        """Cek jumlah member di suatu aktivitas"""
        a = Aktifitas.objects.get(nama_aktivitas="aktivitas1")
        self.assertEqual(a.member_aktivitas.count(), 2)

    def test_valid_detail_aktivitas_page(self):
        """Test apakah halaman untuk detail ada"""
        a = Aktifitas.objects.get(nama_aktivitas="aktivitas1")
        c = Client()
        response = c.get(f"/story_6/detail/{a.id}")
        self.assertEqual(response.status_code, 200)

    def test_tambah_members_page(self):
        """Test apakah halaman untuk tambah member ada"""
        c = Client()
        response = c.get('/story_6/tambah_member/')
        self.assertEqual(response.status_code, 200)

    def test_tambah_aktivitas_page(self):
        """Test apakah halaman untuk tambah aktivitas ada"""
        c = Client()
        response = c.get("/story_6/tambah_aktivitas/")
        self.assertEqual(response.status_code, 200)

    def test_hapus_member(self):
        """Test hapus member"""
        c = Client()
        a = Aktifitas.objects.get(nama_aktivitas="aktivitas1")
        member = Member.objects.get(nama_depan_member = 'nama1')
        member_id = member.id
        response = c.get(f'/story_6/hapus_member/{member_id}')
        self.assertEqual(response.status_code, 302)
        self.assertEqual(a.member_aktivitas.count(), 1)

    def test_hapus_aktivitas(self):
        """Test hapus aktivitas"""
        a = Aktifitas.objects.get(nama_aktivitas="aktivitas2")
        aktivitas_id = a.id
        c = Client()
        response = c.get(f'/story_6/hapus_aktivitas/{aktivitas_id}')
        self.assertEqual(response.status_code, 302)
        self.assertEqual(Aktifitas.objects.count(), 1)
    
    def test_tambah_aktivitas_POST(self):
        c = Client()
        response = c.post('/story_6/tambah_aktivitas/', {
            "deskripsi" : "deskripsi3",
            "nama_aktivitas": "aktivitas3"
        })
        self.assertEqual(response.status_code, 302)
        self.assertEqual(Aktifitas.objects.count(), 3)

    def test_tambah_member_POST(self):
        c = Client()
        aktivitas = Aktifitas.objects.get(deskripsi="deskripsi1")

        response = c.post('/story_6/tambah_member/', {
            "aktivitas": aktivitas.id,
            "nama_depan_member": "daniel",
            "nama_belakang_member": "syahputra",
        })
        self.assertEqual(response.status_code, 302)