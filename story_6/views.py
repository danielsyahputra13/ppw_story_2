from .forms import AktifitasForm, MemberForm
from .models import Aktifitas, Member
from django.shortcuts import redirect, render
from django.contrib import messages

# Create your views here.
def index(request):
    activities = Aktifitas.objects.all()
    return render(request, 'story_6/index.html', {
        "activities" : activities
    })


def tambah_aktivitas(request):
    if request.method == "POST":
        form = AktifitasForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, (f"Aktivitas {request.POST['nama_aktivitas']} berhasil ditambahkan!"))
            return redirect('story_6:index')
    else:
        form = AktifitasForm()
        return render(request, 'story_6/tambah_aktivitas.html', {
            'form': form
        })

def detail(request, pk):
    aktivitas = Aktifitas.objects.get(pk=pk)
    members = Member.objects.filter(aktivitas=aktivitas)
    return render(request, 'story_6/detail.html', {
        'aktivitas': aktivitas, 
        'members': members,
    }) 

def tambah_member(request):
    if request.method == "POST":
        form = MemberForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, (f"{request.POST['nama_depan_member']} {request.POST['nama_belakang_member']} sudah ditambahkan ke aktivitas!"))
            return redirect('story_6:index')
    else:
        form = MemberForm()
        return render(request, 'story_6/tambah_member.html', {
            'form': form
        })

def hapus_member(request, pk):
    member = Member.objects.get(pk=pk)
    messages.success(request, (f"Member {member.nama_depan_member} {member.nama_belakang_member} sudah dihapus!"))
    member.delete()
    return redirect('story_6:index')

def hapus_aktivitas(request, pk):
    aktivitas = Aktifitas.objects.get(pk=pk)
    messages.success(request, (f"Aktivitas {aktivitas.nama_aktivitas} sudah dihapus!"))
    aktivitas.delete()
    return redirect('story_6:index')