from django.db import models
import datetime

# Create your models here.
class Aktifitas(models.Model):
    deskripsi = models.TextField(max_length=500)
    nama_aktivitas = models.CharField(max_length=200)

    def __str__(self):
        return f"Aktifitas: {self.nama_aktivitas}"


class Member(models.Model):
    aktivitas = models.ManyToManyField(Aktifitas, related_name="member_aktivitas")
    nama_depan_member = models.CharField(max_length=100)
    nama_belakang_member = models.CharField(max_length=100)

    def __str__(self):
        return f"{self.nama_depan_member} {self.nama_belakang_member}"