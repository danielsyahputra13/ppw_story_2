from django import forms
from .models import Aktifitas, Member

class AktifitasForm(forms.ModelForm):

    class Meta:
        model = Aktifitas
        fields = '__all__'

class MemberForm(forms.ModelForm):

    class Meta:
        model = Member
        fields = '__all__'