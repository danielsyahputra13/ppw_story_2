from django.urls import path
from . import views

app_name= 'story_6'

urlpatterns = [
    path('', views.index, name='index'),
    path('tambah_aktivitas/', views.tambah_aktivitas, name='tambah_aktivitas'),
    path('detail/<int:pk>', views.detail, name='detail'),
    path('tambah_member/', views.tambah_member, name='tambah_member'),
    path('hapus_member/<int:pk>', views.hapus_member, name='hapus_member'),
    path('hapus_aktivitas/<int:pk>', views.hapus_aktivitas, name='hapus_aktivitas')
]
