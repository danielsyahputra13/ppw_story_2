const getColors = () => {
    const red = Math.floor(Math.random() * 256);
    const green = Math.floor(Math.random() * 256);
    const blue = Math.floor(Math.random() * 256);
    const opacity = Math.random()
    const colorChange = `rgba(${red}, ${green}, ${blue}, ${opacity})`
    return colorChange
}

document.addEventListener('DOMContentLoaded', () => {
    document.querySelectorAll('.block').forEach(block => {
        block.addEventListener('mouseenter', function () {
            const colorChange = getColors();
            block.style.background = colorChange;
            block.style.transition = "background ease-in-out .4s";
            document.querySelector('body').style.background = colorChange;
            document.querySelector('body').style.transition = "background ease-in-out .5s";
        });
        block.addEventListener('mouseleave', function () {
            block.style.background = " #232324";
            document.querySelector('body').style.background = ' #141516';
        })
    })
})
