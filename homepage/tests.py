from django.test import TestCase, Client
from django.urls import reverse

# Create your tests here.
class UrlsPathTest(TestCase):
    def setUp(self) -> None:
        self.homepage = reverse('homepage:home')
    
    def test_homepage_matching_with_the_right_path(self):
        self.assertEqual(self.homepage, '/')

class HomepageTest(TestCase):
    def setUp(self) -> None:
        self.homepage = reverse('homepage:home')
    
    def test_homepage_page_exist(self):
        response = Client().get(self.homepage)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'homepage/index.html')